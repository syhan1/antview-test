# Flutter 플랫폼의 RemoteMonster SDK 기반 Plugin 테스트

This repository is published to explain about a Flutter Plugin View.

```bash
flutter run
```

<img src="./Screenshot_.jpg" width=450 />



Web서버를 이용하여 영상을 송신한후, 모바일(Flutter)에서 수신 테스트
동시에 6~10개 View 사용.

## 활용한 3rd-party library

https://github.com/iktakahiro/flutter_platformview_example


## PlatformView 란 ??

**PlatformView** is a Flutter feature to realize to render Native-UIs thorough Android View/UIKitView.

If you would like to get more information about PlatformView, see official docs:

- API Docs
  - [AndroidView class \- widgets library \- Dart API](https://api.flutter.dev/flutter/widgets/AndroidView-class.html)
  - [UiKitView class \- widgets library \- Dart API](https://api.flutter.dev/flutter/widgets/UiKitView-class.html)
- Awesome Articles
  - [Flutter PlatformView: How to create Widgets from Native Views](https://medium.com/flutter-community/flutter-platformview-how-to-create-flutter-widgets-from-native-views-366e378115b6)
  - [Build your Own Plugin using \(PlatformViews\) — Flutter](https://medium.com/@KarthikPonnam/build-your-own-plugin-using-platformviews-flutter-5b42b4c4fb0a)
