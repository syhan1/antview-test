package com.example.flutter_platformview_example

import android.content.Context
import android.view.LayoutInflater
import android.view.View

import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.platform.PlatformView

import org.webrtc.SurfaceViewRenderer
import com.remotemonster.sdk.RemonCast


class FirstWidget internal constructor(context: Context, id: Int, messenger: BinaryMessenger) : PlatformView, MethodCallHandler {
    private val view: View
    private val methodChannel: MethodChannel

    val surfRendererRemote: SurfaceViewRenderer
    val viewer: RemonCast

    override fun getView(): View {
        return view
    }

    init {
        view = LayoutInflater.from(context).inflate(R.layout.first_widget, null)
        methodChannel = MethodChannel(messenger, "plugins/first_widget_$id")
        methodChannel.setMethodCallHandler(this)

        surfRendererRemote = view.findViewById(R.id.remote_video_view)

        viewer = RemonCast.builder()
                .serviceId("a8624de2-7232-41df-bab7-93dda48fa39e")
                .key("a9880b7b193711c19d3b7111da6cc5ef779f02af38983839bcee5e2495cde52f")
                .context(context)
                .remoteView(surfRendererRemote)
                .build()

        viewer.onJoin(null)
        viewer.join("ANTCH010")
    }

    override fun onMethodCall(methodCall: MethodCall, result: MethodChannel.Result) {
        when (methodCall.method) {
            "ping" -> ping(methodCall, result)
            else -> result.notImplemented()
        }
    }

    private fun ping(methodCall: MethodCall, result: Result) {
        result.success(null)
    }

    override fun dispose() {
    }
}