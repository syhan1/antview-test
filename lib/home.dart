import 'package:flutter/material.dart';
import './first.dart';
import './second.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('앤트썸'),
      ),
      body: GridView.count(
        crossAxisCount: 2,
        children: <Widget>[
          Card(
            child: SizedBox(
              height: 100,
              child: FirstWidget(),
            ),
          ),
          Card(
            child: SizedBox(
              height: 100,
              child: FirstWidget(),
            ),
          ),
          Card(
            child: SizedBox(
              height: 100,
              child: FirstWidget(),
            ),
          ),
          Card(
            child: SizedBox(
              height: 100,
              child: FirstWidget(),
            ),
          ),
          Card(
            child: SizedBox(
              height: 100,
              child: FirstWidget(),
            ),
          ),
          Card(
            child: SizedBox(
              height: 100,
              child: FirstWidget(),
            ),
          ),
        ],
      ),
    );
  }
}
